#pragma once
#include "Helper.h"
#include "Validator.h"
#include "User.h"
#include <vector>
#include <iostream>

using namespace std;

class User;

class Room
{
private:
	vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionsNo;
	string _name;
	int _id;
	string getUsersAsString(vector<User*>, User*);
	void sendMessage(string);
	void sendMessage(User*, string);
	bool removeUser(User*);
	void clearRooms();
public:
	Room(int, User*, string, int, int, int);
	~Room();
	bool joinRoom(User*);
	void leaveRoom(User*);
	int closeRoom(User*);
	vector<User*> getUsers();
	string getUsersListMessage();
	int getQuestionsNo();
	int getQuestionsTime();
	int getID();
	string getName();
	
};

