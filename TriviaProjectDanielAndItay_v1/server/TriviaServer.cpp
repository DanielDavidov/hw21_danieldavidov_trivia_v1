#include "TriviaServer.h"

int TriviaServer::_roomIdSequence = 0;

TriviaServer::TriviaServer()
{
	
	_socket= ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}


TriviaServer::~TriviaServer()
{
	try
	{
		destroyAll();
	}
	catch (...){}
}

void TriviaServer::serve()
{
	bindAndListen();
	while (true) accept();
}

void TriviaServer::destroyAll()
{
	::closesocket(_socket);
	for (std::map<SOCKET, User*>::iterator pair = _connectedUsers.begin(); pair != _connectedUsers.end(); pair++)
	{
		closesocket(pair->first);
		delete(pair->second);
	}
}

void TriviaServer::accept()
{
	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_socket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);
	thread(&TriviaServer::clientHandler, this, client_socket).detach();
}

void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	int port = 8820; /*/*//*/*//*/*///////*/*//*/*//*/*//*/*///////*/*//*/*//*/*//*/*///////*/*//*/*//*/*//*/*///////*/*/
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
}

void TriviaServer::clientHandler(SOCKET client)
{
	int type = 0;
	do
	{
		try{
			type = Helper::getMessageTypeCode(client);
			RecievedMessages* rec = buildRecieveMessage(client, type);
			delete(rec);
		}
		catch (exception& e){ closesocket(client); }
	} while (type != MT_CLIENT_ABORT);
	
}


RecievedMessages* TriviaServer::buildRecieveMessage(SOCKET client , int code){
	cout << code << endl;
	try{
		if (code == MT_CLIENT_SIGN_IN){
			int userLen = Helper::getIntPartFromSocket(client, 2);
			string user = Helper::getStringPartFromSocket(client, userLen);
			int passLen = Helper::getIntPartFromSocket(client, 2);
			string pass = Helper::getStringPartFromSocket(client, passLen);
			vector<string> msg = { user, pass };
			RecievedMessages* ret = new RecievedMessages(client, MT_CLIENT_SIGN_IN, msg);
			handleSignin(ret);
			return ret;
		}
		if (code == MT_CLIENT_SIGN_OUT){
			RecievedMessages* ret = new RecievedMessages(client, MT_CLIENT_SIGN_OUT);
			handleSignout(ret);
			return ret;
		}
		if (code == MT_CLIENT_SIGN_UP)
		{
			int userLen = Helper::getIntPartFromSocket(client, 2);
			string user = Helper::getStringPartFromSocket(client, userLen);
			int passLen = Helper::getIntPartFromSocket(client, 2);
			string pass = Helper::getStringPartFromSocket(client, passLen);
			int emailLen = Helper::getIntPartFromSocket(client, 2);
			string email = Helper::getStringPartFromSocket(client, emailLen);
			vector<string> msg = { user, pass, email };
			RecievedMessages* ret = new RecievedMessages(client, MT_CLIENT_SIGN_UP, msg);
			handleSignup(ret);
			return ret;
		}
		if (code == MT_CLIENT_CREATE_ROOM)
		{
			int nameLen = Helper::getIntPartFromSocket(client, 2);
			string room = Helper::getStringPartFromSocket(client, nameLen);
			int playersNo = Helper::getIntPartFromSocket(client, 1);
			int queNo = Helper::getIntPartFromSocket(client, 2);
			int ansTime = Helper::getIntPartFromSocket(client, 2);
			vector<string> msg = { room, to_string(playersNo),to_string( queNo), to_string(ansTime )};
			RecievedMessages* ret = new RecievedMessages(client, MT_CLIENT_CREATE_ROOM, msg);
			handlCreateRoom(ret);
			return ret;
		}
		if (code == MT_CLIENT_JOIN_ROOM)
		{
			int roomId = Helper::getIntPartFromSocket(client, 4);
			vector<string> msg = { to_string(roomId) };
			cout << endl << endl << "room id " << roomId << endl;
			RecievedMessages* ret = new RecievedMessages(client, MT_CLIENT_JOIN_ROOM,msg);
			handlJoinRoom(ret);
			return ret;
		}
		
		if (code == MT_CLIENT_CLOSE_ROOM)
		{
			RecievedMessages* ret = new RecievedMessages(client, MT_CLIENT_CLOSE_ROOM);
			handlCloseRoom(ret);
			return ret;
		}
		if (code == MT_CLIENT_GET_ROOMS)
		{
			RecievedMessages* ret = new RecievedMessages(client, MT_CLIENT_GET_ROOMS);
			handleGetRooms(ret);
			return ret;
		}
		if (code == MT_CLIENT_GET_USERS)
		{
			int roomId = Helper::getIntPartFromSocket(client, 4);
			RecievedMessages* ret = new RecievedMessages(client, MT_CLIENT_GET_USERS, vector<string>{to_string(roomId)});
			handlGetUsersInRoom(ret);
			return ret;
		}
		if (code == MT_CLIENT_CLOSE_ROOM)
		{
			RecievedMessages* ret = new RecievedMessages(client, MT_CLIENT_CLOSE_ROOM);
			handlCloseRoom(ret);
			return ret;
		}
		if (code == MT_CLIENT_EXIT_ROOM)
		{
			RecievedMessages* ret = new RecievedMessages(client, MT_CLIENT_EXIT_ROOM);
			if(!handleLeaveRoom(ret)) cout << "An error in leaving room occourd! "<< endl;
			return ret;
		}
		
	}
	catch (exception&e){ cout << e.what(); }
};

void TriviaServer::handlGetUsersInRoom(RecievedMessages*rcv){
	User* user = getUserBySocket(rcv->getSock());
	int id = stoi(rcv->getValues()[0]);
	Room* room = getRoomById(id);
	if (room == nullptr){
		cout << endl << "There is no room by id !!!!! problem " << endl;
		user->send(to_string(MT_SERVER_GET_USERS) + "0");
	}
	else	 
	{
		string toSend = room->getUsersListMessage();
		user->send(toSend);
	};

};

User* TriviaServer::handleSignin(RecievedMessages* rcv){
	string user = rcv->getValues()[0];
	string pass = rcv->getValues()[1];
	SOCKET sock = rcv->getSock();
	User* check = getUserByName(user);

	if (check != nullptr){
		check->send(to_string(MT_SERVER_SIGN_IN) + "2"); // users already connected
		return nullptr;
	}
	if (_db.isUserExists(user)) {
		cout << "user exists!!!" << endl;
		if (_db.isUserAndPassMatch(user, pass)) {
			cout << "and password matches user" << endl;
			check = new User(user, sock);
			_connectedUsers.insert(std::pair<SOCKET, User*>(sock, check));
			check->send("1020"); // success
			cout << "-----------------\nThe user: " << user << " joined successfully" << endl;
			return check;
		}
	}

	check = new User(user, sock);
	check->send("1021"); // wrong details
	delete check;
	return nullptr;
	
};

bool TriviaServer::handleSignup(RecievedMessages* rcv)
{
	try{
		string user = rcv->getValues()[0];
		string pass = rcv->getValues()[1];
		string email = rcv->getValues()[2];
		User* check = getUserByName(user);
		SOCKET soc = rcv->getSock();

		bool userOk = Validator::isUsernameValid(user);
		bool passOk = Validator::isPasswordValid(pass);
		bool userExist = _db.isUserExists(user);
		bool added = false;

		if (check == nullptr) check = new User(user, soc); // there is no connected user
		else userExist = true;

		if (!passOk) check->send("1041");
		else if (!userOk) check->send("1043");
		else if (userExist) check->send("1042");
		else{
			if (added = _db.addNewUser(user, pass, email) == false) check->send("1044"); // other
			check->send("1040"); // success
		}
		return(!added || !userOk || userExist || !passOk);
	}
	catch (exception& e){ cout << e.what(); }
}

bool TriviaServer::handlCreateRoom(RecievedMessages* rcv){
	SOCKET userSock = rcv->getSock(); // I found that socket is ok
	User* user = getUserBySocket(userSock);
	if (user == nullptr) {
		cout << "There is no user" << endl;
		User* def = new User("default", userSock);
	    def->send(to_string(MT_SERVER_CREATE_ROOM) + "1"); // failure
		delete (def);
		return false;
	}
	string name = rcv->getValues()[0];
	int playersNo = stoi(rcv->getValues()[1]);
	int queNo = stoi(rcv->getValues()[2]);
	int queTime = stoi(rcv->getValues()[3]);

	if (user->createRoom(TriviaServer::_roomIdSequence, name, playersNo, queNo, queTime))
	{
		_roomsList.insert(std::pair<int, Room*>(TriviaServer::_roomIdSequence, user->getRoom()));
		printRooms();
		user->send(to_string(MT_SERVER_CREATE_ROOM) + "0"); // success 
		TriviaServer::_roomIdSequence++;
	}
	else{ user->send(to_string(MT_SERVER_CREATE_ROOM) + "1"); } // failure
};

void TriviaServer::printRooms(){
	cout << "rooms list now size(" << _roomsList.size() << ") is: " << endl;
	for (std::map<int, Room*>::iterator it = _roomsList.begin(); it != _roomsList.end(); it++)
	{
		cout << it->second->getID() << ") " << it->second->getName() << endl;
	}
};

bool TriviaServer::handlJoinRoom(RecievedMessages* rcv){
	User* user = getUserBySocket(rcv->getSock());
	// Handle null user
	if (user == nullptr) {
		cout << "no user" << endl;
		user = new User("default", rcv->getSock());
		user->send(to_string(MT_SERVER_JOIN_ROOM) + "2"); // failure
		delete(user);
		return false;
	}
	int id = stoi(rcv->getValues()[0]);
	Room* room = getRoomById(id);
	if (room == nullptr) cout << "can't join" << endl;
	printRooms();
	
	if (user->joinRoom(room)){
		string message = to_string(room->getQuestionsNo()) + to_string(room->getQuestionsTime());
		user->send(to_string(MT_SERVER_JOIN_ROOM) + "0" + message);
		return true;
	}
	else { // can't join the room
		user->send(to_string(MT_SERVER_JOIN_ROOM) + "1");
		return false;
	}
	user->send(to_string(MT_SERVER_CREATE_ROOM) + "2"); // failure
	return false;
};


void TriviaServer::handleGetRooms(RecievedMessages* rcv){
	User* user = getUserBySocket(rcv->getSock());
	string toSend = to_string(MT_SERVER_GET_ROOMS);
	string id = to_string(TriviaServer::_roomIdSequence);
	
	if(_roomsList.size() !=0) toSend += Validator::carryZeroesInFrontOfString(id, 4);
	else toSend += "0";
	for (map<int, Room*>::iterator it = _roomsList.begin(); it != _roomsList.end(); it++)
	{
		string room_id = Validator::carryZeroesInFrontOfString(to_string(it->first),4);
		string name = it->second->getName();
		string nameSize = Validator::carryZeroesInFrontOfString(to_string(name.length()), 2);
		toSend += room_id + nameSize + name;
	}
	user->send(toSend);
};

bool TriviaServer::handlCloseRoom(RecievedMessages* rcv){
	cout << "closes rooms! " << endl;
	User* user = getUserBySocket(rcv->getSock());
	Room* room = user->getRoom();
	if (room == nullptr) return false;
	int id = room->getID();
	
	if (user->closeRoom() != -1){
		_roomsList.erase(id);
		printRooms();
		return true; // success
	};
	return false;
};

bool TriviaServer::handleLeaveRoom(RecievedMessages*rcv){
	User* user = getUserBySocket(rcv->getSock());
	Room* room = user->getRoom();
	if (room == nullptr) return false;
	user->leaveRoom();
	return true;
};


void TriviaServer::handleSignout(RecievedMessages* rcv)
{
	User* user = getUserBySocket(rcv->getSock());
	Room* room = user->getRoom();
	if (user == nullptr) return; // user must be signed in!
	if (room != nullptr){
		if(user->closeRoom()==-1) 
			room->leaveRoom(user);
	}
	_connectedUsers.erase(rcv->getSock());
}

User* TriviaServer::getUserByName(string name)
{
	for (std::map<SOCKET, User*>::iterator pair = _connectedUsers.begin(); pair != _connectedUsers.end(); pair++)
	{
		if (pair->second->getUsername() == name) return  pair->second;
	}
	return nullptr;
}

User* TriviaServer::getUserBySocket(SOCKET sock){
	return (_connectedUsers[sock]);
};

Room* TriviaServer::getRoomById(int id){
	return _roomsList[id];
};