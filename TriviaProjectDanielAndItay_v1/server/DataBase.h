#pragma once
#include <iostream>
#include <vector>
#include <string>
#include "sqlite3.h"
#include "Question.h"

using namespace std;
class DataBase
{
public:
	DataBase();
	~DataBase();
	bool isUserExists(string);
	bool addNewUser(string, string, string);
	bool isUserAndPassMatch(string, string);
	vector<Question*> initQuestion(int);
	vector<string> getBestScores();
	vector<string> getPersonalStatus(int);
	int insertNewGame();
	bool updateGameStatus(int);
	bool addAnswerToPlayer(int, string, int, string, bool, int);
private:
	sqlite3* db;
	string path;
	int rc;
	char *zErrMsg = 0;

	void handleException(int rc);
	bool checkTableExists(string name);

	static int callbackCount(void*, int, char**, char**);
	static int callbackQuestions(void*, int, char**, char**);
	static int callbackBestScores(void*, int, char**, char**);
	static int callbackPersonalStatus(void*, int, char**, char**);
};

