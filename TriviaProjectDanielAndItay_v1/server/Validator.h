#pragma once
#include <string>
#include <iostream>
using namespace std;

class Validator
{
public:
	static bool isPasswordValid(string);
	static bool isUsernameValid(string);
	static string carryZeroesInFrontOfString(string id, int overallLen);

};