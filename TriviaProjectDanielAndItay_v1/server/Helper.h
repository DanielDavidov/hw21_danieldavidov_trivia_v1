#pragma once
#pragma comment(lib, "Ws2_32.lib")
#include <vector>
#include <WinSock2.h>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

enum MessageType : byte
{
	MT_CLIENT_SIGN_IN = 200,
	MT_CLIENT_SIGN_OUT = 201,
	MT_CLIENT_SIGN_UP = 203,
	MT_CLIENT_GET_ROOMS = 205,
	MT_CLIENT_GET_USERS = 207,
	MT_CLIENT_JOIN_ROOM = 209,
	MT_CLIENT_EXIT_ROOM = 211,
	MT_CLIENT_CREATE_ROOM = 213,
	MT_CLIENT_CLOSE_ROOM = 215,
	MT_CLIENT_START_GAME = 217,
	MT_CLIENT_CLIENT_ANS = 219,
	MT_CLIENT_EXIT_GAME = 222,
	MT_CLIENT_GET_BEST_SCORE = 223,
	MT_CLIENT_GET_STATE = 225,
	MT_CLIENT_ABORT = 299,

	MT_SERVER_SIGN_IN = 102,
	MT_SERVER_SIGN_UP = 104,
	MT_SERVER_GET_ROOMS = 106,
	MT_SERVER_GET_USERS = 108,
	MT_SERVER_JOIN_ROOM = 110,
	MT_SERVER_EXIT_ROOM = 112,
	MT_SERVER_CREATE_ROOM = 114,
	MT_SERVER_CLOSE_ROOM = 116,
	MT_SERVER_START_GAME = 118,
	MT_SERVER_CLIENT_ANS = 120,
	MT_SERVER_EXIT_GAME = 121,
	MT_SERVER_GET_BEST_SCORE = 124,
	MT_SERVER_GET_STATE = 126
};



class Helper
{
public:
	 static int getMessageTypeCode(SOCKET sc);
	 static char* getPartFromSocket(SOCKET sc, int bytesNum, int flags);
	 static int getIntPartFromSocket(SOCKET sc, int bytesNum);
	 static std::string getStringPartFromSocket(SOCKET sc, int bytesNum);
	 static void sendData(SOCKET sc, std::string message);
	 static std::string getPaddedNumber(int num, int digits);
	
	// static std::string carryZeroesInFrontOfString(string id, int overallLen);
private:
	static char* Helper::getPartFromSocket(SOCKET sc, int bytesNum);

};


#ifdef _DEBUG // vs add this define in debug mode
#include <stdio.h>
// Q: why do we need traces ?
// A: traces are a nice and easy way to detect bugs without even debugging
// or to understand what happened in case we miss the bug in the first time
#define TRACE(msg, ...) printf(msg "\n", __VA_ARGS__);
// for convenient reasons we did the traces in stdout
// at general we would do this in the error stream like that
// #define TRACE(msg, ...) fprintf(stderr, msg "\n", __VA_ARGS__);

#else // we want nothing to be printed in release version
#define TRACE(msg, ...) printf(msg "\n", __VA_ARGS__);
#define TRACE(msg, ...) // do nothing
#endif