#pragma once
#include "Helper.h"
#include "DataBase.h"
#include "User.h"
#include <map>
#include <iostream>
#include <vector>
using namespace std;

class User;

class Game
{
private:
	map <std::string, int> _results;
	int _currentTurnAnswers;
	vector <Question*> _questions;
	vector <User*> _players;
	int _questions_no;
	int currQuestionIndex;
	DataBase& db;
	bool insertGameToDB();
	void initQuestionsFromDB();
	void sendQuestionToAllUsers();
public:
	Game(const vector<User*>&, int, DataBase&);
	~Game();
	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswerFromUser(User*, int, int);
	bool leaveGame(User*);
	int getID();
};
