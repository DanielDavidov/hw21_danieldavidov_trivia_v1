#pragma once
#include "Helper.h"
#include "Room.h"
#include "Game.h"

class Room;
class Game;

using namespace std;
class User
{
private:
	string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;
public:
	User(string,SOCKET);
	~User();
	void send(string);
	string getUsername();
	SOCKET getSocket();
	Room* getRoom();
	void setGame(Game*);
	void clearGame();
	bool createRoom(int, string, int, int, int);
	bool joinRoom(Room*);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();
	void clearRoom();
	string toString();
};

