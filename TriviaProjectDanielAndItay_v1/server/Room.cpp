#include "Room.h"


Room::Room(int id, User* admin, string name, int maxUsers, int questionsNo, int questionTime) : _id(id), _admin(admin), _name(name), _maxUsers(maxUsers), _questionsNo(questionsNo), _questionTime(questionTime){
	_users.push_back(admin);
}


Room::~Room()
{

}

bool Room::joinRoom(User* add)
{
	string toSend = to_string(MT_SERVER_JOIN_ROOM);
	if (_maxUsers == _users.size()) {
		toSend += "1";
		add->send(toSend);
		return false;
	}
	try{
		string num = Validator::carryZeroesInFrontOfString(to_string(_questionsNo),2);
		string time = Validator::carryZeroesInFrontOfString(to_string(_questionTime),2);
		toSend += "0" + num + time;
		cout << endl << "toSend is " << toSend << endl;
		_users.push_back(add);
		add->send(toSend);
		cout << "Sending Messages:" << endl << endl;
		sendMessage(getUsersListMessage());
		return true;
	}
	catch (exception& e){ cout << e.what(); system("pause"); exit(1); }
}

void Room::leaveRoom(User* user)
{
	if (removeUser(user)) sendMessage(to_string(MT_SERVER_EXIT_ROOM) + "0");
}

bool Room::removeUser(User* user)
{
	bool succeed = false;
	vector<User*> users;
	for (int i = 0; i < _users.size(); i++){
		if (user == _users[i]) {
			succeed = true;
			continue;
		}
		users.push_back(users[i]);
	}
	_users = users;
	return succeed;
}

int Room::closeRoom(User* user)
{
	if (_admin != user) return -1;
	sendMessage(to_string(MT_SERVER_CLOSE_ROOM));
	clearRooms();
}

void Room::clearRooms(){
	for (int i = 0; i < _users.size(); i++) 
		_users[i]->clearRoom();
}

vector<User*> Room::getUsers()
{
	return _users;
}

string Room::getUsersListMessage()
{
	string ret = to_string(MT_SERVER_GET_USERS);

	// after user add to this if game started, the manager closed room - 0 and return it
	if (_admin->getRoom() == nullptr) return ret + "0"; // theoreticly can't be!!!
	
	ret += to_string(_users.size());
	for (int i = 0; i < _users.size(); i++)
	{
		string name = _users[i]->getUsername();
		ret += Validator::carryZeroesInFrontOfString(to_string(name.length()),2) + name;
	}
	return ret;
}

int Room::getQuestionsNo()
{
	return _questionsNo;
}

int Room::getQuestionsTime(){
	return _questionTime;
}

int Room::getID()
{
	return _id;
}

string Room::getName()
{
	return _name;
}

string Room::getUsersAsString(vector<User*> users, User* exc)
{
	string ret;
	for (int i = 0; i < users.size(); i++) ret += users[i]->toString() + "\n";
	return ret;
}

void Room::sendMessage(string toSend)
{
	sendMessage(nullptr, toSend);
}

void Room::sendMessage(User* exclude, string toSend)
{
	
	for (int i = 0; i < _users.size(); i++)
	{
		if (_users[i] == exclude) continue;
		try{
			_users[i]->send(toSend);
		}
		catch (exception& e){ cout << e.what(); system("pause"); exit(1); }
	}
}
