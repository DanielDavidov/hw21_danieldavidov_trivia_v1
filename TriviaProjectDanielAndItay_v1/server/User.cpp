#include "User.h"


User::User(string username, SOCKET sock)
{
	_currGame = nullptr;
	_currRoom = nullptr;
	_username = username;
	_sock = sock;
}


User::~User()
{
}

SOCKET User::getSocket()
{
	return _sock;
}

string User::getUsername()
{
	return _username;
}

void User::send(string s)
{
	cout << "sending: " << s << endl;
	Helper::sendData(_sock, s);
}

Room* User::getRoom(){
	return _currRoom;
};
void User::setGame(Game* game){
	_currGame = game;
	_currRoom = nullptr;
};
void User::clearGame(){
	_currGame = nullptr;
};
bool User::createRoom(int id, string room, int maxUsers, int queNo, int queTime){ 
	
	if (_currRoom != nullptr) {
		send(to_string(MT_SERVER_CREATE_ROOM) + "1");
		return false;
	}
	User* admin = this;
	cout << endl<< "createRoom: " << admin->getUsername() << "id: " << id << endl;
	_currRoom = new Room(id, admin, room, maxUsers, queNo, queTime);
	send(to_string(MT_SERVER_CREATE_ROOM) + "0");
	return true; 
};

bool User::joinRoom(Room* join){
	User* user = this;
	return join->joinRoom(user);
};

void User::leaveRoom(){
	User* user = this;
	_currRoom->leaveRoom(user);
	_currRoom = nullptr;
};

int User::closeRoom(){ 
	if (_currRoom == nullptr) return -1; // user not assigned to a room
	User* user = this;
	return _currRoom->closeRoom(user); // can return -1 if it's not admin
};
bool User::leaveGame(){ 
	User* user = this;
	return _currGame->leaveGame(user);
};

void User::clearRoom()
{
	_currRoom = nullptr;
}

string User::toString(){
	return _username; //+// " curr room: " + ""

}