#include "Game.h"

Game::Game(const vector<User*>& users, int num, DataBase& data) : db(data), _players(users)
{
	if (!data.insertNewGame()) throw("couldn't insert new game");
	data.initQuestion(num);
	_players = users;
	for (int i = 0; i < users.size(); i++)		
		users[i]->setGame(this);
}


Game::~Game()
{

}

bool Game::leaveGame(User*)
{
	return false;
}

/*
void Game::sendFirstQuestion()
{

}

void Game::handleFinishGame()
{

}

bool Game::handleNextTurn()
{

}

bool Game::handleAnswerFromUser(User*, int, int)
 {

}

int Game::getID()
{

}

bool Game::insertGameToDB()
{

}

void Game::initQuestionsFromDB()
{

}
void Game::sendQuestionToAllUsers()
{

}*/