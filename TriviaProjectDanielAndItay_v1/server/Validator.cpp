#pragma once
#include <iostream>
#include <string>
#include "Validator.h"
using namespace std;

string Validator::carryZeroesInFrontOfString(string id, int overallLen){
	
	int len = id.length();
	for (int i = 0; i < overallLen - len; i++) id = '0' + id;
	
	return id;
};

bool Validator::isPasswordValid(string pass)
{
	int numFlag=0, capitalFlag=0, underFlag = 0;
	if (pass.length() < 4)		return false;
	
	for (int i = 0; i < pass.length(); i++)
	{
		if (pass[i] == ' ') 
		{
			return false;
		}
	}
	
	for (int i = 0; i < pass.length(); i++)
	{
		if (pass[i] >= 'A' && pass[i] <= 'Z')
		{
			capitalFlag++;
		}
		if (pass[i] >= 'a' && pass[i] <= 'z')
		{
			underFlag++;
		}
		if (pass[i] >= '0' && pass[i] <= '9')
		{
			numFlag++;
		}
	}
	if (capitalFlag > 0 && numFlag > 0 && underFlag > 0)
	{
		return true;
	}
	return false;
}

bool Validator::isUsernameValid(string user)
{
	if (user == "")
	{
		return false;
	}
	if (user[0] < 'A' && user[0] > 'Z' && user[0] < 'a' && user[0] > 'z')
	{
		return false;
	}
	for (int i = 0; i < user.length(); i++)
	{
		if (user[i] == ' ')
		{
			return false;
		}
	}
	return true;
}