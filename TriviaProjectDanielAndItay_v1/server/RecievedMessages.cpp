#include "RecievedMessages.h"


RecievedMessages::RecievedMessages(SOCKET client, int type)
{
	_sock = client;
	_messageCode = type;
	_user = nullptr;
}

RecievedMessages::RecievedMessages(SOCKET client, int type, vector<string> msgs){
	_sock = client;
	_messageCode = type;
	_values = msgs;
	_user = nullptr;
};

SOCKET RecievedMessages::getSock()
{
	return _sock;
}

User* RecievedMessages::getUser(){
	return _user;
}

void RecievedMessages::setUser(User* set)
{
	_user = set;
}

int RecievedMessages::getMessageCode()
{
	return _messageCode;
}

vector<string>& RecievedMessages::getValues()
{
	return _values;
}

RecievedMessages::~RecievedMessages()
{
}
