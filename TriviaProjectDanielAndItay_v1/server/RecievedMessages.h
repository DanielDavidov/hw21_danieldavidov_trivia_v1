#pragma once
#include "Helper.h"
//#include "User.h"
class User;
using namespace std;
class RecievedMessages
{
private:
	SOCKET _sock;
	User* _user;
	int _messageCode;
	vector<string> _values;
public:
	RecievedMessages(SOCKET,int );
	RecievedMessages(SOCKET, int, vector<string>);
	~RecievedMessages();
	SOCKET getSock();
	User* getUser();
	void setUser(User*);
	int getMessageCode();
	vector<string>& getValues();

};

