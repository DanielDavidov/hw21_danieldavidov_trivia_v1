#pragma once
#include "Helper.h"
#include "sqlite3.h"

#include "Validator.h"
#include "DataBase.h"
#include "Room.h"
#include "RecievedMessages.h"
#include <thread>
#include <map>
#include <queue>
#include <mutex>
#include <iostream>
using namespace std;

class TriviaServer
{
private: 
	SOCKET _socket;
	map<SOCKET, User*> _connectedUsers;
	DataBase _db;
	map<int, Room*> _roomsList;

	mutex _mtxRecievedMessages;
	queue <RecievedMessages*> _queRcvMessages;
	static int _roomIdSequence;

public:
	TriviaServer();
	~TriviaServer();

	void serve();
private:
	void printRooms();
	void destroyAll();
	void bindAndListen();
	void accept();
	void clientHandler(SOCKET);
	void safeDeleteUser(RecievedMessages*);

	User* handleSignin(RecievedMessages*);////////
	bool handleSignup(RecievedMessages*);
	void handleSignout(RecievedMessages*);

	void handleLeaveGame(RecievedMessages*);
	void handleStartGame(RecievedMessages*);
	void handlePlayerAnswer(RecievedMessages*);

	bool handlCreateRoom(RecievedMessages*);
	bool handlCloseRoom(RecievedMessages*);
	bool handlJoinRoom(RecievedMessages*);
	bool handleLeaveRoom(RecievedMessages*);

	void handlGetUsersInRoom(RecievedMessages*);
	void handleGetRooms(RecievedMessages*);

	void handleGetBestScores(RecievedMessages*);
	void handleGetPersonalStatus(RecievedMessages*);

	void handleRecievedMessages();
	//void addRecievedMessage(RecievedMessages*);

	RecievedMessages* buildRecieveMessage(SOCKET, int);

	User* getUserByName(string);
	User* getUserBySocket(SOCKET);
	Room* getRoomById(int);
};

